# Slovenian translation of Drupal (modules/poll.module)
# Copyright Tadej Baša <tadej.basa@gmail.com>, Iztok Jug <iztokj@gmail.com>
# Generated from file: poll.module,v 1.162.2.1 2005/05/31 21:13:39 unconed
# 
msgid ""
msgstr ""
"Project-Id-Version: 5.0\n"
"POT-Creation-Date: 2007-01-30 12:39+0100\n"
"PO-Revision-Date: 2006-06-20 20:59+0100\n"
"Last-Translator: David <davidlicen@gmail.com>\n"
"Language-Team: Slovenian <tadej.basa@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"
"X-Poedit-Language: Slovenian\n"
"X-Poedit-Country: SLOVENIA\n"
"X-Poedit-SourceCharset: utf-8\n"

#: modules/poll/poll.module:16
msgid "The poll module can be used to create simple polls for site users. A poll is a simple multiple choice questionnaire which displays the cumulative results of the answers to the poll. Having polls on the site is a good way to get instant feedback from community members."
msgstr ""

#: modules/poll/poll.module:17
msgid "Users can create a poll. The title of the poll should be the question, then enter the answers and the \"base\" vote counts. You can also choose the time period over which the vote will run.The <a href=\"@poll\">poll</a> item in the navigation menu will take you to a page where you can see all the current polls, vote on them (if you haven't already) and view the results."
msgstr ""

#: modules/poll/poll.module:18
msgid "For more information please read the configuration and customization handbook <a href=\"@poll\">Poll page</a>."
msgstr ""

#: modules/poll/poll.module:40
msgid "Most recent poll"
msgstr "Novejše ankete"

#: modules/poll/poll.module:105
msgid "Negative values are not allowed."
msgstr "Negativne vrednosti niso dovoljene."

#: modules/poll/poll.module:110
msgid "You must fill in at least two choices."
msgstr "Vnesti morate vsaj dve možnosti."

#: modules/poll/poll.module:147
msgid "Choices"
msgstr "Izbire"

#: modules/poll/poll.module:159
msgid "Need more choices"
msgstr "Potrebujete več izbir"

#: modules/poll/poll.module:161
msgid "If the amount of boxes above isn't enough, check this box and click the Preview button below to add some more."
msgstr ""

#: modules/poll/poll.module:168
#, fuzzy
msgid "Choice @n"
msgstr "Izbira %n"

#: modules/poll/poll.module:175
#, fuzzy
msgid "Votes for choice @n"
msgstr "Glasovi za izbiro %n"

#: modules/poll/poll.module:184
msgid "Closed"
msgstr "Zaprto"

#: modules/poll/poll.module:191
msgid "Poll status"
msgstr "Status ankete"

#: modules/poll/poll.module:194
msgid "When a poll is closed, visitors can no longer vote for it."
msgstr "Obiskovalci ne morejo glasovati na anketo, ki je zaprta."

#: modules/poll/poll.module:199
msgid "Poll duration"
msgstr "Čas veljavnosti ankete"

#: modules/poll/poll.module:202
msgid "After this period, the poll will be closed automatically."
msgstr "Po določenem obdobje, bo anketa avtomatsko zaprta."

#: modules/poll/poll.module:234
#, fuzzy
msgid "Polls"
msgstr "Anketa"

#: modules/poll/poll.module:240;380;488;609
#, fuzzy
msgid "Vote"
msgstr "1 glas"

#: modules/poll/poll.module:260
#, fuzzy
msgid "Votes"
msgstr "1 glas"

#: modules/poll/poll.module:268;602
msgid "Results"
msgstr ""

#: modules/poll/poll.module:322
msgid "A poll is a multiple-choice question which visitors can vote on."
msgstr "Anketa je vprašanje, na katero obiskovalci lahko odgovorijo z izbiro enega od predlogov."

#: modules/poll/poll.module:323
msgid "Question"
msgstr "Vprašanje"

#: modules/poll/poll.module:336
msgid "open"
msgstr "odpri"

#: modules/poll/poll.module:336
msgid "closed"
msgstr "zaprto"

#: modules/poll/poll.module:428;435
msgid "Total votes: %votes"
msgstr "Vseh glasov: %votes"

#: modules/poll/poll.module:447
msgid "Cancel your vote"
msgstr ""

#: modules/poll/poll.module:485
msgid "This table lists all the recorded votes for this poll.  If anonymous users are allowed to vote, they will be identified by the IP address of the computer they used when they voted."
msgstr ""

#: modules/poll/poll.module:534
msgid "Your vote was recorded."
msgstr "Tvoj glas je bil oddan."

#: modules/poll/poll.module:537
#, fuzzy
msgid "You are not allowed to vote on this poll."
msgstr "Nimate pravic za glasovanje v tej anketi."

#: modules/poll/poll.module:541
#, fuzzy
msgid "You did not specify a valid poll choice."
msgstr "V anketi niste pravilno označili opcije."

#: modules/poll/poll.module:571
#, fuzzy
msgid "Your vote was canceled."
msgstr "Tvoj glas je bil oddan."

#: modules/poll/poll.module:574
#, fuzzy
msgid "You are not allowed to cancel an invalid poll choice."
msgstr "Nimate pravic za glasovanje v tej anketi."

#: modules/poll/poll.module:600
#, fuzzy
msgid "Older polls"
msgstr "starejše ankete"

#: modules/poll/poll.module:600
msgid "View the list of polls on this site."
msgstr "Prikaži seznam vseh anket, ki se nahajajo na tej strani."

#: modules/poll/poll.module:602
msgid "View the current poll results."
msgstr "Prikaži trenutne rezultate ankete."

#: modules/poll/poll.module:336;414
msgid "1 vote"
msgid_plural "@count votes"
msgstr[0] "1 glas"
msgstr[1] "%count glasova"
msgstr[2] "%count glasovi"
msgstr[3] "%count glasov"

#: modules/poll/poll.module:347
msgid "create polls"
msgstr "ustvari anketo"

#: modules/poll/poll.module:347
msgid "vote on polls"
msgstr "glasuj v anketi"

#: modules/poll/poll.module:347
msgid "cancel own vote"
msgstr ""

#: modules/poll/poll.module:347
msgid "inspect all votes"
msgstr ""

#: modules/poll/poll.module:0
msgid "poll"
msgstr "anketa"

#~ msgid "Settings"
#~ msgstr "Nastavitve"

#~ msgid "polls"
#~ msgstr "ankete"

#, fuzzy
#~ msgid "vote"
#~ msgstr "1 glas"

#, fuzzy
#~ msgid "votes"
#~ msgstr "1 glas"

#~ msgid "You have to specify a question."
#~ msgstr "Morate določiti vprašanje."
